# Dan Taylor
# Find CVE details given CVE ID

# native
import glob
import json



def make_cve_dicts(j):
	"""
	Returns dict with CVE details.
	
	:j: dict, read from CVE JSON
	"""
	
	dicts = {}
	for cve in j['CVE_Items']:
		cve_id = cve['cve']['CVE_data_meta']['ID']
		dicts[cve_id] = cve
		
	return dicts

def read_cve_details(filename):
	"""
	Returns dict with CVE details.
	Designed to read JSON files from NVE database
	
	:filename: string
	"""
	with open(filename) as f:
		j = json.load(f)
		return make_cve_dicts(j)

def make_db(filepath):
	"""
	Makes a dict 'database' from NVE JSON files.
	
	:filepath: string, finds all NVE JSON in this directory
	"""
	db = {}
	for f in glob.glob(filepath + 'nvdcve-1.1-[0-9]*.json'):
		temp_db = read_cve_details(f)
		for i in temp_db:
			db[i] = temp_db[i]
	return db
	


# Read in database
db = make_db(filepath='')

details = []
with open('CVEs') as f:

	# Find details for each CVE in my file
	for line in f:
		cve_id = line.strip()
		if cve_id in db:
			details.append(db[cve_id])

# Write relevant CVE to file
with open('relevant_cve.json', 'w') as f:
	json.dump(details, f)
