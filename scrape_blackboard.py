# Dan Taylor
# Scrape everything from all your blackboard classes... For testing only!

# native
import json
import logging
import os

# pypi
import requests
from bs4 import BeautifulSoup


def rest(endpoint, verb='GET', return_json=True, url=None):
    """
    Use global requests session r
    :endpoint: string like "v1/users/userName:dtaylor/courses"
    :verb: string HTTP verb like PUT or GET
    :return_json: boolean set to True if returning JSON from response
    :url: string if set makes the REST call to that URL ignoring base URL
    """
    if not url:
        url = base_url + endpoint
    response = r.request(verb, url)

    if return_json:
        try:
            # In a try block because sometimes there is no JSON
            return response.json()

        except Exception as e:
            logging.error(e)
            return None

    # Returning the response object
    return response


def get_content(cid, parents=[]):
    """
    Recursively get course content and save to file. 
    Mimics course folder structure.

    :cid: string content id, like _170555_1
    :parents: parent content ids, like ["_170555_1", "_171123_1"]
    """
    content = rest(f"v1/courses/{parents[0]}/contents/{cid}")
    if not content:
        return

    path = '/'.join(parents)

    # A file?
    if 'hasChildren' not in content:

        if 'contentHandler' not in content:

            logging.warning(f"no_handler {cid}")
            return

        logging.debug(f"{cid} {content['contentHandler']['id']}")

        if content['contentHandler']['id'] == 'resource/x-bb-document':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-file':
            attachments = rest(
                f"v1/courses/{parents[0]}/contents/{cid}/attachments")
            for attachment in attachments['results']:
                response = rest(
                    f"v1/courses/{parents[0]}/contents/{cid}/attachments/{attachment['id']}/download", return_json=False)

                with open(f"{path}/{attachment['fileName']}", 'wb') as f:
                    f.write(response.content)

        elif content['contentHandler']['id'] == 'resource/x-bb-courselink':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-toollink':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-assignment':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-asmt-test-link':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-forumlink':
            pass

        elif content['contentHandler']['id'] == 'resource/x-osv-kaltura/mashup':
            pass

        elif content['contentHandler']['id'] == 'resource/x-bb-video':
            return  # will implement this other stuff later!
            body = content['body']
            try:
                soup = BeautifulSoup(body)
                li = soup.li
                link = li.a['href']
                filename = li.a.text
                response = rest('override', return_json=False, url=link)

                with open(f"{path}/{filename}", 'wb') as f:
                    f.write(response.content)
            except Exception as e:
                logging.error(e)
            pass

        else:
            logging.warning(
                f"unhandled_type {cid} type={content['contentHandler']['id']}")

        return

    # A folder
    if content['hasChildren'] is True:

        os.mkdir(f"{path}/{cid}")
        children = rest(f"v1/courses/{parents[0]}/contents/{cid}/children")
        with open(f"{path}/{cid}/children.json", 'w') as f:
            json.dump(children, f)

        # recursion!
        parents.append(cid)
        for child in children['results']:
            get_content(child['id'], parents)


logging.basicConfig(level=logging.INFO)

r = requests.Session()
r.headers = {
    'JSESSIONID': '...',
    'Cookie': 'JSESSIONID=...; session_id=...; s_session_id=...; JSESSIONID=...; r12smSESSION=...; web_client_cache_guid=...',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:76.0) Gecko/20100101 Firefox/76.0'
}

base_url = 'https://blackboard.jhu.edu/learn/api/public/'

courses = r.get(base_url + 'v1/users/userName:dtaylor/courses').json()
with open('courses.json', 'w') as f:
    json.dump(courses, f)


course_ids = [r['courseId'] for r in courses['results']]
course_details = []
for cid in course_ids:
    response = r.get(base_url + f"v1/courses/{cid}")
    details = response.json()
    course_details.append(details)


for course in course_details:
    os.mkdir(course['id'])
    contents = r.get(f"{base_url}v1/courses/{course['id']}/contents").json()
    with open(f"{course['id']}/contents.json", 'w') as f:
        json.dump(contents, f)

    for content in contents['results']:
        get_content(content['id'], parents=[course['id']])


'''
response = r.get(base_url + 'v1/courses/_197329_1/contents/_7571933_1/attachments/_6440387_1/download')

with open('response.json', 'w') as f:
    json.dump(response.json(), f)
'''
