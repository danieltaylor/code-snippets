// Dan Taylor
// Freenove intro to Arduino


int ledOne = 4;
int ledTwo = 5;

void setup() {
  // put your setup code here, to run once:
  pinMode(ledOne, OUTPUT);
  pinMode(ledTwo, OUTPUT);
  
}

void loop() {
  // put your main code here, to run repeatedly:
  setLed(LOW, HIGH, 100);
  setLed(HIGH, LOW, 100);
  setLed(HIGH, HIGH, 200);
  setLed(LOW, LOW, 200); 
  setLed(HIGH, HIGH, 200);
  setLed(LOW, LOW, 200);
  setLed(LOW, HIGH, 100);
  setLed(HIGH, LOW, 100);
  setLed(LOW, HIGH, 100);
  setLed(HIGH, LOW, 100);  
}

void setLed(int led1, int led2, int delayTime){
  digitalWrite(ledOne, led1);
  digitalWrite(ledTwo, led2);
  delay(delayTime);
}
