// Dan Taylor
// Freenove intro to Arduino
// Analog to Digital Converter potentiometer

int adc;
float voltage;

void setup() {
  Serial.begin(9600);
  Serial.println("We're converting here!");
}

void loop() {
  adc = analogRead(A0);
  voltage = adc * (5.0 / 1023.0); // Careful, somestimes cheap potentiometers catch on fire... 
  Serial.print("adc=");
  Serial.println(adc);
  Serial.print("voltage=");
  Serial.println(voltage); // Reached voltage 1023, and it caught on fire!
  delay(400);
}