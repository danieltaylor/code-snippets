import subprocess

file="f5.out"
i=0
ASCII="ASCII"
bzip = "bzip"
gzip = "gzip"
bashCommand = "file f5.out"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
while True:	
	print("starting loop: "+str(i))
	bashCommand = "file "+str(i)
	process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
	output, error = process.communicate()
	if (ASCII in output):
		print("\tfile is ASCII")
		bashCommand = "base64 --decode "+str(i)
		i += 1
		print("\t"+bashCommand)
		open(str(i), 'a').close()
		f = open(str(i),"w")
		process = subprocess.Popen(bashCommand.split(), stdout=f)
		output, error = process.communicate()
		process = ''
		output = ''
		continue
	if (bzip in output):
		print("\tfile is bzip")
		bashCommand = "bzip2 -d "+str(i)
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		bashCommand = "mv "+str(i)+".out "
		i += 1
		bashCommand += str(i)
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		process = ''
		output = ''
		continue

	if (gzip in output):
		print("\tfile is gzip")
		bashCommand = "mv "+str(i)+" "+str(i)+".gz"
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		process = ''
		output = ''
		bashCommand = "gunzip "+str(i)+".gz"
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		process = ''
		output = ''
		bashCommand = "cp "+str(i)+" "
		i += 1
		bashCommand += str(i)
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		process = ''
		output = ''
		continue

	if (" Zip" in output):
		print("\tfile is Zip")
		bashCommand = "unzip -o "+str(i)
		print("\t" + bashCommand)
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		i += 1
		bashCommand = "mv flag "+str(i)
		print("\t" + bashCommand)
		process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
		output, error = process.communicate()
		continue
		
	print(output)
	print("Finshing loop: "+str(i))
	
	
