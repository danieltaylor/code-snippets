

/* 
 *  Create a WiFi access point and provide a web server on it.
 *  Serves temperature and humidity log
*/

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>  // Serving CSV
#include <LittleFS.h>          // Saving CSV
#include "DHTesp.h"            // Temp and humidity


/*Set up temp and humidity sensor */
DHTesp dht;
const int UPDATE_INTERVAL_SECONDS = 2;



/* Set up WiFi */
#ifndef APSSID
#define APSSID "loglog"
#define APPSK  "REDACTED"
#endif
const char *ssid = APSSID;
const char *password = APPSK;
ESP8266WebServer server(80);


int count = 0;

void readFile(const char * path) {
  Serial.printf("Reading file: %s\n", path);

  File file = LittleFS.open(path, "r");
  if (!file) {
    Serial.println("Failed to open file for reading");
    return;
  }

  Serial.print("Read from file: ");
  while (file.available()) {
    Serial.write(file.read());
  }
  file.close();
}

void writeFile(const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = LittleFS.open(path, "w");
  if (!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if (file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  delay(2000); // Make sure the CREATE and LASTWRITE times are different
  file.close();
}

void appendFile(const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = LittleFS.open(path, "a");
  if (!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if (file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}






// For testing
void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected!</h1>");
}

// Serves CSV file
void handleCSV() {
  File f = LittleFS.open("/test.csv", "r");
  server.streamFile(f, "text/csv");
  f.close();
}





void setup() {
  delay(1000);
  Serial.begin(115200);
  Serial.println();
  Serial.print("Configuring access point...");
  /* You can remove the password parameter if you want the AP to be open. */
  
  WiFi.softAP(ssid, password);
  server.on("/", handleRoot);
  server.on("/test.csv", handleCSV);
  server.begin();

  // Start file system
  LittleFS.format();
  if (!LittleFS.begin()) {
    Serial.println("LittleFS mount failed");
    return;
  }


  // Start sensor
  dht.setup(D6, DHTesp::DHT11);

  // Create demo file
  writeFile("/test.csv", "seconds,temp,humid\n");
  
  

}

void loop() {
  
  if(count % 100 == 0){
    float humi = dht.getHumidity();
    float temp = dht.getTemperature();
    String l = String(count * UPDATE_INTERVAL_SECONDS) + "," + String(temp) + "," + String(humi) + "\n";
    Serial.println(l);
    appendFile("/test.csv", l.c_str());
  }
  
  server.handleClient();
  delay(1000 * UPDATE_INTERVAL_SECONDS);
  count++;
}