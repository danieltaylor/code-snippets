// Dan Taylor
// Freenove intro to Arduino
// Control an LED using photoresistor input

int photo;
int led = 9;

void setup() {
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  Serial.print('Reading photo data from pin');
  Serial.println(led);
}

void loop() {
  photo = analogRead(A0);
  photo = map(photo, 0, 1023, 0, 255);

  // Tell me what the value is
  Serial.println(photo);

  // Write to LED
  analogWrite(led, photo);

  delay(35);
}