// Dan Taylor
// Freenove intro to Arduino
// Analog control of RGB

// Output pins
int red = 11;
int green = 10;
int blue = 9;

// Input pins
int redInput = A0;
int greenInput = A1;
int blueInput = A2;

void setup() {
  Serial.begin(9600);
  delay(500);
  Serial.println("Welcome to RGB CITY!");

  setPin("red", red);
  setPin("green", green);
  setPin("blue", blue);
}

// Set pin to OUTPUT and print the name
void setPin(char pinName[], int pinNum){
  pinMode(pinNum, OUTPUT);
  Serial.print(pinName);
  Serial.print(":");
  Serial.println(pinNum);
}

// Convert analog to digital from in, write to out
void write_adc(char color[], int in, int out){
  int adc = analogRead(in);
  adc = map(adc, 0, 1023, 0, 255);
  Serial.print(color);
  Serial.print(":");
  Serial.println(adc);
  analogWrite(out, adc);
}

void loop() {
  write_adc("red", redInput, red);
  write_adc("green", greenInput, green);
  write_adc("blue", blueInput, blue);
  delay(500);
}