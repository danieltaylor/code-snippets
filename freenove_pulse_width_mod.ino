// Daniel Taylor
// Freenove intro to Arduino
// Dim LEDs with Pulse Width Modulation

// Only pins with ~ can do PWM
int p1 = 5;
int p2 = 6;
int p3 = 9;
int p4 = 10;
int p5 = 11;
int p6 = 3;

void setup() {
  pinMode(p1, OUTPUT);
  pinMode(p2, OUTPUT);
  pinMode(p3, OUTPUT);
  pinMode(p4, OUTPUT);
  pinMode(p5, OUTPUT);
  pinMode(p6, OUTPUT);
}

void loop() {
  // Set the pins to different brightness
  // map() function changes scale of a ratio
  drain(p1, p2, p3, p4, p5, p6);
}

void fade(int p, int dtime) {
  // Fade the LED out
  for (int i = 255; i > 0; i--){
    analogWrite(p, i);
    delay(dtime);
  }
}

void drain(int p1, int p2, int p3, int p4, int p5, int p6){
  // Drains the health bar
  int pins[] = {p1, p2, p3, p4, p5, p6};
  for(int i = 0; i <= sizeof(pins); i++){
    analogWrite(pins[i], 255);
  }
  delay(100);
  
  for (int i = sizeof(pins); i >= 0; i--){
    fade(pins[i], 2);
    analogWrite(pins[i], 255);
  }
}