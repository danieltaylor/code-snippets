// Dan Taylor
// Freenove intro to Arduino
// Control motor with potentiometer, L239D IC

int channelOne = 10;
int channelTwo = 9;
int enable = 11;

bool rotationDir;
int rotationSpeed;

void setup(){
  pinMode(channelOne, OUTPUT);
  pinMode(channelTwo, OUTPUT);
  pinMode(enable, OUTPUT);
}


// Adjust motor speed baesd on potentiometer input
void loop(){
  
  int pVal = analogRead(A0);  // Read potentiometer

  rotationSpeed = pVal - 512;
  if (pVal > 512){
    rotationDir = true;
  } else {
    rotationDir = false;
  }

  setMotor(rotationDir, map(abs(rotationSpeed), 0, 512, 0, 255));
}


// Set the motor speed and direction
void setMotor(bool rotationDir, int rotationSpeed){

  int one = 0;
  int two = 1;
  
  if (rotationDir){ 
    one = 1;
    two = 0;
  } 

  digitalWrite(channelOne, one);
  digitalWrite(channelTwo, two);
  analogWrite(enable, constrain(rotationSpeed, 0, 255));
}
