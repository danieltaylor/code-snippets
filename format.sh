#!/bin/bash

# Saves time by formatting clipboard.

# I made flash cards out of PDF based 
# testing material and this automated
# the formatting!

pbpaste | sed -Ee 's/\(A\)/\
\
\(A\)/g' | sed -Ee 's/\(B\)/\
\(B\)/g' | sed -Ee 's/\(C\)/\
\(C\)/g' | sed -Ee 's/\(D\)/\
\(D\)/g' | sed -Ee 's/\(E\)/\
\(E\)   /g' | sed -Ee 's/\(F\)/\
\(F\)/g' | sed -Ee 's/\(G\)/\
\(G\)/g' | pbcopy